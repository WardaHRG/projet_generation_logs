# PROJET_GENERATION_LOGS



Le but de ce projet est de constituer un système de génération de logs. Ces logs seront destinés à être utilisés par des DataScientists.
Afin de mieux présenter les éléments, on commencera par définir les composantes de l'architecture globale. Ensuite, on présentera les différents composants utilisés. Pour finir, on présentera les livrables à fournir lors de la soutenance.
Le système de collecte des données est découpé en plusieurs parties :
Dans un premier temps, on distingue 2 Raspberry interconnectés en utilisant le protocole TX/RX.
D'un côté, l'un des Raspberry qu'on appellera "Balise" doté de différents capteurs se charge de collecter régulièrement les informations fournies par les capteurs.
De l'autre côté, le second Raspberry qu'on appellera "Maître" via le protocole TX/RX récupère ces données.
Il va de soi qu'en cas de redémarrage de la "Balise", le programme de collecte des informations devra se relancer automatiquement.
Concernant le "Maître", l'intervalle de collecte doit rester paramétrable. A ces données issues des capteurs, on y ajoutera les caractéristiques liées à la balise elle-même.
Une fois ces données récupérées par le "Maître", celles-ci doivent être formatées au format json en vue de les envoyer à un serveur TCP.
Ce serveur TCP centralisera les différentes données transmises par le "Maître" à intervalle régulier afin de permettre d'établir des statistiques exploitables par les dataScientists.

Par ailleurs, le Maître sera doté de 2 servo-moteurs positionnés sur les connecteurs 1 et 2 de la carte Contrôleur. Chaque servo moteur sera doté d'un drapeau. Sur l'un, on dispose d'un drapeau de couleur Jaune, et sur l'autre de couleur Bleue.
Chaque servo-moteur aura 5 positions.

Servo Moteur : Drapeau Jaune
1 : 0° ➔ Position Repos (Aucune présence détectée)
2 : 45° ➔ Néant
3 : 90 ° ➔ Détection Présence
4 : 135° ➔ Néant
5 : 180° ➔Position Repos (Après Présence détectée)

Servo Moteur : Drapeau Bleu
1 : 0° ➔ Néant
2 : 45° ➔ Détection de Nouvelles données (par le Maître) Position 2 vers Position 4
3 : 90 ° ➔ Transfert vers Serveur TCP
4 : 135° ➔ Détection de Nouvelles données (par le Maître) Position 4 vers Position 2
5 : 180° ➔ Position Repos (Etat normal)


De plus, en vue de faciliter l'exploitation des données par les Datascientists, vous constituerez une application console exposant les différentes informations issues du serveur TCP.
Au sein de cette application console, on pourrait par exemple :
- Obtenir les informations à un instant donné
o d'un capteur
o ou d'une information particulière du capteur
- Extraire des informations pour une période donnée
- …
Il s'agit ici de simples suggestions, à vous de fournir une application console regroupant des fonctionnalités utiles aux datascientists. Cette application doit être simple et conviviale.

Voici les détails concernant les différents éléments :
- Raspberry Pi 3 Model B+
- Capteur d'humidité BME280
- Capteur Triple axis-boussole magnétomètre - HMC5883L ou GY-521
- Détecteur de mouvement infrarouge HC-SR501
- Carte PCA9685 Contrôleur de Servo Moteur

Afin de disposer de ces données, vous aurez la possibilité de vous connecter via le protocole SSH. Toutes les informations de connexion (adresse IP, login et mot de passe, clé) vous seront communiquées ultérieurement.
L’ensemble des travaux réalisés devra être mis sur un dépôt afin de centraliser les informations et de faciliter la communication entre les membres du projet. Vous ferez en sorte de bien vous répartir les tâches.
Il va de soi que le code doit être respecté les règles d'écriture. Vous devrez générer une documentation.