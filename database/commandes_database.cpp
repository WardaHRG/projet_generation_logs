#include "commandes_database.hpp"

int humidite()
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    //printf("Base ouverte avec succes.\n");
    cout << "---------------------------------" << endl;

    char* requete = "select ID, Date, Heure, Humidite from data where ID = (select max(ID) from data);";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "HUMIDITE = " << sqlite3_column_text(stmt, 3) << " %" << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}


int temperature(bool kel)
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    //printf("Base ouverte avec succes.\n");
    cout << "---------------------------------" << endl;

    char* requete = "select ID, Date, Heure, Temperature from data where ID = (select max(ID) from data);";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);

    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        if(kel==false){
            cout << "TEMPERATURE = " << sqlite3_column_text(stmt, 3) << " Degres Celsius" << endl;}
        else{
        //temp = sqlite3_column_text(stmt, 3);
            const char* temp_const = (const char*)temp;
//cout << "temp_const = " << temp_const << endl;
            int t = atof(temp_const);
//cout << "t = " << t << endl;
            int t_kelvin = t + 273.15;
            printf("TEMPERATURE : %d Kelvin\n", t_kelvin);
        }
    }
/*
int temp_size = 0;
while(temp[temp_size] != '\0') {
    ++temp_size;
cout << "compteur = " << temp_size<< endl;
cout << "temp[] = " << temp[temp_size] << endl;}
char temp_char[temp_size];
for (int i=0; i<temp_size; i++) {
    temp_char[i] = static_cast<char>(temp[i]);
cout << "temp_char[i] = " << temp_char[i] << endl;
}
*/

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}

int pression()
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    //printf("Base ouverte avec succes.\n");
    cout << "---------------------------------" << endl;

    char* requete = "select ID, Date, Heure, Pression from data where ID = (select max(ID) from data);";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "PRESSION = " << sqlite3_column_text(stmt, 3) << " bar" << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}

int mouvements()
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    //printf("Base ouverte avec succes.\n");
    cout << "---------------------------------" << endl;

    char* requete = "select ID, Date, Heure, Mouvement from data where ID = (select max(ID) from data);";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "DETECTEUR : " << sqlite3_column_text(stmt, 3) << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}

int position()
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    //printf("Base ouverte avec succes.\n");
    cout << "---------------------------------" << endl;

    char* requete = "select ID, Date, Heure, X, Y, Z from data where ID = (select max(ID) from data);";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "POSITION x : " << sqlite3_column_text(stmt, 3) << endl;
        cout << "POSITION y : " << sqlite3_column_text(stmt, 4) << endl;
        cout << "POSITION z : " << sqlite3_column_text(stmt, 5) << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}



int pression_periode(int period_p)
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }

    sqlite3_stmt* stmt_eff = NULL;
    char* requete_effectif = "select max(ID) from data";
    sqlite3_prepare_v2(db, requete_effectif, -1, &stmt_eff, NULL);
    sqlite3_step(stmt_eff);
    int effectif = sqlite3_column_int(stmt_eff, 0);
    sqlite3_finalize(stmt_eff);
    /*TEST ERREUR PERIODE HORS INTERVALLE
    if(period_p>effectif){
        throw Exception(CausesErreurs::ERR_PERIODE_HORS_INTERVALLE);}
        */
    //CALCUL DU NOMBRE D'INCREMENTS a FAIRE POUR REPONDRE A DEMANDE DU USER
    string nbstr = to_string(period_p-1);
    const char* nbch = nbstr.c_str();

    cout << "---------------------------------" << endl;

    const char* requete = "select ID, Date, Heure, Pression from data where ID between (select max(ID) from data)-? and (select max(ID) from data)";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, nbch, strlen(nbch), NULL);

    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "Pression = " << sqlite3_column_text(stmt, 3) << " bar" << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}



int position_periode(int period_pos)
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }

    sqlite3_stmt* stmt_eff = NULL;
    char* requete_effectif = "select max(ID) from data";
    sqlite3_prepare_v2(db, requete_effectif, -1, &stmt_eff, NULL);
    sqlite3_step(stmt_eff);
    int effectif = sqlite3_column_int(stmt_eff, 0);
    sqlite3_finalize(stmt_eff);
    /*TEST ERREUR PERIODE HORS INTERVALLE
    if(period_pos>effectif){
        throw Exception(CausesErreurs::ERR_PERIODE_HORS_INTERVALLE);}
        */
    //CALCUL DU NOMBRE D'INCREMENTS a FAIRE POUR REPONDRE A DEMANDE DU USER
    string nbstr = to_string(period_pos-1);
    const char* nbch = nbstr.c_str();

    cout << "---------------------------------" << endl;

    const char* requete = "select ID, Date, Heure, X, Y, Z from data where ID between (select max(ID) from data)-? and (select max(ID) from data)";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, nbch, strlen(nbch), NULL);

    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "POSITION x : " << sqlite3_column_text(stmt, 3) << endl;
        cout << "POSITION y : " << sqlite3_column_text(stmt, 4) << endl;
        cout << "POSITION z : " << sqlite3_column_text(stmt, 5) << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}


int  mouvements_periode(int period_m)
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }

    sqlite3_stmt* stmt_eff = NULL;
    char* requete_effectif = "select max(ID) from data";
    sqlite3_prepare_v2(db, requete_effectif, -1, &stmt_eff, NULL);
    sqlite3_step(stmt_eff);
    int effectif = sqlite3_column_int(stmt_eff, 0);
    sqlite3_finalize(stmt_eff);
    /*TEST ERREUR PERIODE HORS INTERVALLE
    if(period_m>effectif){
        throw Exception(CausesErreurs::ERR_PERIODE_HORS_INTERVALLE);}
    */
    //CALCUL DU NOMBRE D'INCREMENTS a FAIRE POUR REPONDRE A DEMANDE DU USER
    string nbstr = to_string(period_m-1);
    const char* nbch = nbstr.c_str();

    cout << "---------------------------------" << endl;

    const char* requete = "select ID, Date, Heure, Mouvement from data where ID between (select max(ID) from data)-? and (select max(ID) from data)";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, nbch, strlen(nbch), NULL);

    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "DETECTION : " << sqlite3_column_text(stmt, 3) << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}

int humidite_periode(int period_h)
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }

    sqlite3_stmt* stmt_eff = NULL;
    char* requete_effectif = "select max(ID) from data";
    sqlite3_prepare_v2(db, requete_effectif, -1, &stmt_eff, NULL);
    sqlite3_step(stmt_eff);
    int effectif = sqlite3_column_int(stmt_eff, 0);
    sqlite3_finalize(stmt_eff);
    /*TEST ERREUR PERIODE HORS INTERVALLE
    if(period_h>effectif){
        throw Exception(CausesErreurs::ERR_PERIODE_HORS_INTERVALLE);}
        */
    //CALCUL DU NOMBRE D'INCREMENTS a FAIRE POUR REPONDRE A DEMANDE DU USER
    string nbstr = to_string(period_h-1);
    const char* nbch = nbstr.c_str();

    cout << "---------------------------------" << endl;

    const char* requete = "select ID, Date, Heure, Humidite from data where ID between (select max(ID) from data)-? and (select max(ID) from data)";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, nbch, strlen(nbch), NULL);

    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        cout << "HUMIDITE = " << sqlite3_column_text(stmt, 3) << " %" << endl;}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}




int temperature_periode(int period_t, bool kel2)
{
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open("logsDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    //RECUPERATION DU NOMBRE TOTAL DE RELEVES DANS LA DATABASE
    sqlite3_stmt* stmt_eff = NULL;
    char* requete_effectif = "select max(ID) from data";
    sqlite3_prepare_v2(db, requete_effectif, -1, &stmt_eff, NULL);
    sqlite3_step(stmt_eff);
    int effectif = sqlite3_column_int(stmt_eff, 0);
    sqlite3_finalize(stmt_eff);
    /*TEST ERREUR PERIODE HORS INTERVALLE
    if(period_t>effectif){
        throw Exception(CausesErreurs::ERR_PERIODE_HORS_INTERVALLE);}
        */
    //CALCUL DU NOMBRE D'INCREMENTS a FAIRE POUR REPONDRE A DEMANDE DU USER
    string nbstr = to_string(period_t-1);
    const char* nbch = nbstr.c_str();

    cout << "---------------------------------" << endl;

    const char* requete = "select ID, Date, Heure, Temperature from data where ID between (select max(ID) from data)-? and (select max(ID) from data)";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);
    sqlite3_bind_text(stmt, 1, nbch, strlen(nbch), NULL);

    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d\n", sqlite3_column_int(stmt, 0));
        printf("Date : %s\n", sqlite3_column_text(stmt, 1));
        printf("Heure : %s\n", sqlite3_column_text(stmt, 2));
        if(kel2==false){
            cout << "TEMPERATURE = " << sqlite3_column_text(stmt, 3) << " Degres Celsius" << endl;}
        else{
            temp = sqlite3_column_text(stmt, 3);
            const char* temp_const = (const char*)temp;
            int t = atof(temp_const);
            int t_kelvin = t + 273.15;
            printf("TEMPERATURE : %d Kelvin\n", t_kelvin);
        }
    }
/*
int temp_size = 0;
while(temp[temp_size] != '\0') {
    ++temp_size;
cout << "compteur = " << temp_size<< endl;
cout << "temp[] = " << temp[temp_size] << endl;}
char temp_char[temp_size];
for (int i=0; i<temp_size; i++) {
    temp_char[i] = static_cast<char>(temp[i]);
cout << "temp_char[i] = " << temp_char[i] << endl;
}
*/
    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}


/*
int temperature(bool kel){
    sqlite3 *db=NULL;
    sqlite3_stmt *stmt=NULL;

    int rc;
    rc = sqlite3_open(".db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
       return 1;
    }
    printf("Base ouverte avec succes.\n");
    cout << "---------------------------------" << endl;

    char* requete = "select ID, Date, Heure, Temperature from data where ID = (select max(ID) from data);";
    sqlite3_prepare_v2(db, requete, -1, &stmt, NULL);
    const unsigned char* temp;

    while ( (rc = sqlite3_step(stmt)) == SQLITE_ROW)
    {
        printf("ID : %d", sqlite3_column_int(stmt, 0));
        printf("\nDate : %s", sqlite3_column_text(stmt, 1));
        printf("\nHeure : %s", sqlite3_column_text(stmt, 2));
        temp = sqlite3_column_text(stmt, 3);
    }

    const char* temp_const = (const char*)temp;

    if(kel==false){
        printf("\nTEMPERATURE : %s �C", temp);}
    else{
        int t = atoi(temp_const);
        int t_kelvin = t + 273.15;
        printf("\nTEMPERATURE : %d K", t_kelvin);}

    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;}
    return 0;
}
*/
