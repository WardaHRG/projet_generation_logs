#ifndef COMMANDES_DATABASE_HPP_INCLUDED
#define COMMANDES_DATABASE_HPP_INCLUDED

#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <stdlib.h>

//#include "exceptions.hpp"
#include "sqlite3.h"
using namespace std;

int humidite();
int temperature(bool kel);
int pression();
int mouvements();
int position();
int temperature_periode(int, bool);
int humidite_periode(int);
int pression_periode(int);
int position_periode(int);
int mouvements_periode(int);

#endif // COMMANDES_DATABASE_HPP_INCLUDED
