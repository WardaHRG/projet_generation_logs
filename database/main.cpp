#include <iostream>
#include <ctime>
#include <sstream>
#include <vector>

#include "commandes_database.hpp"
//#include "../sqlite-amalgamation-3400000/sqlite3.h"
#include "sqlite3.h"
using namespace std;


int main()
{
   //str += "RASPBERRY PI 3 Model B+. Marque : U:Create. Processeur : ARM. Vitesse du processeur : 1.40 GHz. Coeurs : 4. Taille de la m�moire vive : 1GB. Type de technologie sans fil : 802.11bgn, 802.11ac. Nombre de ports USB 2.0 : 4\n"

    bool kelv = true;
    temperature(!kelv);
    //temperature(kelv);
    humidite();
    pression();
    mouvements();
    position();
    temperature_periode(3, !kelv);
    humidite_periode(2);
    pression_periode(4);
    position_periode(3);
    mouvements_periode(4);

    return 0;
}
