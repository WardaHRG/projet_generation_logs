/*
//  main.cpp
//  ProjetFinalAJC
//  Created by Cheikh Tidiane MBAYE on 11/01/2023.

#include <stdio.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <termios.h>
#include <vector>
#include <fcntl.h>
#include <cstring>
#include <iostream>
#include "bme280.hpp"
*/


//
//  main.cpp
//  ProjetFinalAJC
//  Created by Cheikh Tidiane MBAYE on 11/01/2023.

#include <stdio.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <termios.h>
#include <vector>
#include <fcntl.h>
#include <cstring>
#include <iostream>
#include "bme280.hpp"
#include <wiringPi.h>
#include "i2c.hpp"

//#include <i2c.hpp>
//#include <bme280.h>


#define LEN_MESSAGE 200
#define PIR_PIN 17

using namespace std;

int main()
{
    wiringPiSetupGpio(); 
    pinMode(PIR_PIN, INPUT);

    char adresse = 0x76;    // l'adresse du composant BME280
    bme280 capteur(adresse);
    if (capteur.obtenirErreur()){
        cout << "Capteur BME 280 présent sur le bus I2C" << endl;
        cout <<  hex << capteur.obtenirChipID() << endl;
        capteur.donnerAltitude(62);


        while(true){
            //Detection de Mouvement

            if(!digitalRead(PIR_PIN))
            {
                printf("MOUVEMENT\n");


                //Envoi des donnees au  MAITRE
                int fd, len = 0;

                string str = "Temperature : " + to_string(capteur.obtenirTemperatureEnC()) + " °C"
                ", Pression : " + to_string(capteur.obtenirPression()) + "hPa "
                ", Humidite : " + to_string(capteur.obtenirHumidite()) + " % ";
                const char* message = str.c_str();

                struct termios options;

                fd = open ("/dev/ttyS0", O_RDWR | O_NDELAY | O_NOCTTY);
                if(fd < 0)
                {
                    perror("Erreur d'ouverture du port");
                }

                options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
                options.c_iflag = IGNPAR;
                options.c_oflag = 0;
                options.c_lflag = 0;

                tcflush(fd, TCIFLUSH);
                tcsetattr(fd, TCSANOW, &options);
                usleep(500000);

                // printf("Chaine : %s\n", chaine);
                cout << "Chaine : " << message << endl;
                write(fd, message, LEN_MESSAGE);


                sleep(1);
                system("clear");

            }
            else
            {
                printf("PAS DE MOUVEMENT\n");
		//system("clear");
            }
        }
    }
    else{
        cout << " Le capteur BME280 n'est pas présent à l'adresse : 0x" << hex << (int)adresse << endl;
        capteur.version();
    }
    return 0;
}

