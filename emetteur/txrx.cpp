#include <stdio.h>
#include <string>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <termios.h>
#include <vector>
#include <fcntl.h>
#include <cstring>
#include <iostream>
#include <ctime>
#include <sstream>
#include "bme280.hpp"
#include <wiringPi.h>

#include "i2c.hpp"
#include "bme280.hpp"

#define LEN_MESSAGE 200
#define PIR_PIN 17

using namespace std;

int main()
{
    wiringPiSetupGpio();
    pinMode(PIR_PIN, INPUT);
    
    char adresse = 0x76;    // l'adresse du composant BME280
    bme280 capteur(adresse);
    if (!capteur.obtenirErreur()){
        cout << "Capteur BME 280 présent sur le bus I2C" << endl;
        cout <<  hex << capteur.obtenirChipID() << endl;
        capteur.donnerAltitude(62);
    }
    else{
        cout << " Le capteur BME280 n'est pas présent à l'adresse : 0x" << hex << (int)adresse << endl;
        capteur.version();
        }
    
        
        while(true){
            
                time_t now = time(0);
                tm* localt = localtime(&now);

                ostringstream oss;
		ostringstream oss1;
                oss  << localt->tm_mday << "/" << localt->tm_mon + 1 << "/" << localt->tm_year + 1900;
                oss1 << localt->tm_hour << ":" << localt->tm_min << ":" << localt->tm_sec ;

            
                
                //Envoi des donnees au  MAITRE
                int fd, len = 0;
            
                
                string str = oss.str() + ";" + oss1.str() + ";" + to_string(capteur.obtenirTemperatureEnC()) + ";"
                 + to_string(capteur.obtenirPression()) + ";"
                 + to_string(capteur.obtenirHumidite()) + ";";
                
            
                if(digitalRead(PIR_PIN))
                {
                    printf("MOOUVEMENT\n");
                    str += "MOVE";
                    
                }
		else
		{
			printf("PAS DE MOUVEMENT\n");
			str +="NO MOVE";
		}
                
                const char* message = str.c_str();
                
                struct termios options;
                
                fd = open ("/dev/ttyS0", O_RDWR | O_NDELAY | O_NOCTTY);
                if(fd < 0)
                {
                    perror("Erreur d'ouverture du port");
                }
                
                options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
                options.c_iflag = IGNPAR;
                options.c_oflag = 0;
                options.c_lflag = 0;
                
                tcflush(fd, TCIFLUSH);
                tcsetattr(fd, TCSANOW, &options);
                usleep(1000000);
                
                // printf("Chaine : %s\n", chaine);
                cout << "Chaine : " << message << endl;
                write(fd, message, LEN_MESSAGE);
                
                
                sleep(1);
                //system("clear");
                
         }
    
    return 0;
}

