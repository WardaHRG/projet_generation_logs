#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>

#define PIR_PIN 17

int main()
{
	wiringPiSetupGpio();
	pinMode(PIR_PIN, INPUT);

	while(1)
	{
		if(digitalRead(PIR_PIN))
		{
			printf("MOUVEMENT\n");
		}
		else
		{
			printf("PAS DE MOUVEMENT\n");}
		delay(100);
	}
	return 0;
}
