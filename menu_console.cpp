#include <iostream>

using namespace std;


    cout << "-------------------------------------------------------" << endl;
    cout << "BIENVENUE DANS VOTRE BASE DE DONNEES CAPTEURS PHYSIQUES" << endl;
    cout << "-------------------------------------------------------" << endl;

    int rep;
    do
    {
        cout << "VEUILLEZ SAISIR VOTRE CHOIX PARMIS LES ACTIONS DISPONIBLES : " << endl;
        cout << "1 = Afficher la TEMPERATURE actuelle" << endl;
        cout << "2 = Afficher le TAUX d'HUMIDITE actuel" << endl;
        cout << "3 = Afficher la PRESSION actuelle" << endl;
        cout << "4 = Afficher les informations du DETECTEUR DE MOUVEMENT" << endl;
        cout << "5 = Afficher la POSITION actuelle" << endl;
        cout << "6 = Afficher l'HISTORIQUE des relev�s de TEMPERATURE pour une p�riode" << endl;
        cout << "7 = Afficher l'HISTORIQUE des relev�s de TAUX d'HUMIDITE pour une p�riode" << endl;
        cout << "8 = Afficher l'HISTORIQUE des relev�s de PRESSION pour une p�riode" << endl;
        cout << "9 = Afficher l'HISTORIQUE des relev�s de POSITIONS pour une p�riode" << endl;
        cout << "10 = Afficher l'HISTORIQUE des relev�s de DETECTION DE MOUVEMENT" << endl;
        cout << endl << "0 = Quitter" << endl;

        scanf("%d", &rep);

        switch(rep)
        {
            case 1:
                cout << "---------------------------------" << endl;
                cout << "SOUHAITEZ VOUS LA TEMPERATURE EN KELVIN ? o/n" << endl;
                char k;
                fflush(stdin);
                cin >> k;
                bool kel = false;
                if(k=='o' || k=='O') { kel = true; }
                temperature(kel);
                break;
            case 2:
                cout << "---------------------------------" << endl;
                humidite();
                break;
            case 3:
                cout << "---------------------------------" << endl;
                pression();
                break;
            case 4:
                cout << "---------------------------------" << endl;
                mouvements();
                break;
            case 5:
                cout << "---------------------------------" << endl;
                position();
                break;
            case 6:
                int period_t;
                cout << "VEUILLEZ SAISIR LA PERIODE DE L'HISTORIQUE SOUHAITEE"
                fflush(stdin);
                cin >> period_t;
                cout << "SOUHAITEZ VOUS LA TEMPERATURE EN KELVIN ? o/n" << endl;
                char k2;
                fflush(stdin);
                cin >> k2;
                bool kel2 = false;
                if(k2=='o' || k2=='O') { kel2 = true; }
                try
                {
                    temperature_periode(period_t, kel2);
                }
                catch(const Exception& ex)
                {
                    cout << endl << ex.what() << endl;
                }
                catch(...)
                {
                    cout << "AUTRE ERREUR" << endl;
                }
                break;
            case 7:
                int period_h;
                cout << "VEUILLEZ SAISIR LA PERIODE DE L'HISTORIQUE SOUHAITEE (format [])"
                fflush(stdin);
                cin >> period_h;
                try
                {
                    humidite_periode(period_h);
                }
                catch(const Exception& ex)
                {
                    cout << endl << ex.what() << endl;
                }
                catch(...)
                {
                    cout << "AUTRE ERREUR" << endl;
                }
                break;
            case 8:
                int period_p;
                cout << "VEUILLEZ SAISIR LA PERIODE DE L'HISTORIQUE SOUHAITEE (format [])"
                fflush(stdin);
                cin >> period_p;
                try
                {
                    pression_periode(period_p);
                }
                catch(const Exception& ex)
                {
                    cout << endl << ex.what() << endl;
                }
                catch(...)
                {
                    cout << "AUTRE ERREUR" << endl;
                }
                break;
            case 9:
                int period_pos;
                cout << "VEUILLEZ SAISIR LA PERIODE DE L'HISTORIQUE SOUHAITEE (format [])"
                fflush(stdin);
                cin >> period_pos;
                try
                {
                    position_periode(period_pos);
                }
                catch(const Exception& ex)
                {
                    cout << endl << ex.what() << endl;
                }
                catch(...)
                {
                    cout << "AUTRE ERREUR" << endl;
                }
                break;
            case 10:
                int period_m;
                cout << "VEUILLEZ SAISIR LA PERIODE DE L'HISTORIQUE SOUHAITEE (format [])"
                fflush(stdin);
                cin >> period_m;
                try
                {
                    mouvements_periode(period_m);
                }
                catch(const Exception& ex)
                {
                    cout << endl << ex.what() << endl;
                }
                catch(...)
                {
                    cout << "AUTRE ERREUR" << endl;
                }
                break;
            case 0:
                continue;
            default:
                break;

        }

        cout << "VEUILLEZ SAISIR VOTRE CHOIX PARMIS LES ACTIONS DISPONIBLES : " << endl;

        }while(rep != 0);
