#include <iostream>
#include <fstream>
#include <ios>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string>
#include "pca9685.h"
#include "pca9685.c"
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>

#define PIN_BASE 300
#define HERTZ 50

using namespace std;

const int PORT = 8080; // Port utilisé pour la communication entre BALISE ET MASTER
const char* IP_ADDRESS = "57.128.34.154"; // Adresse IP du SERVEUR TCP PRINCIPAL
const int PORT_TCP_PRINCIPAL = 3124; // Port utilisé pour la communication entre MASTER et SERVEUR TCP PRINCIPAL


int main() {
    // Création de la socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        std::cerr << "Error creating socket" << std::endl;
        return -1;
    }
cout << "socket created" << endl;
        // Configuration de l'adresse locale
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    serv_addr.sin_addr.s_addr = INADDR_ANY;
cout << "adresse locale configuree" << endl;
        // Association de l'adresse à la socket
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << "Error binding socket" << std::endl;
        return -1;
    }
cout << "socket binded" << endl;
    // Mise en écoute de la socket
cout << "begin listen..." << endl;
        listen(sockfd, 5);

        // Acceptation d'une connexion
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0) {
        cerr << "Error accepting connection" << endl;
        return -1;
    }
    cout << "CONNECTION FROM BALISE ACCEPTED" << endl;

    
    //CONFIGURATION CONNEXION entre MASTER et SERVEUR TCP PRINCIPAL
    // Création de la socket pour TCP PRINCIPAL
    int sockfd2 = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd2 < 0) {
        printf("Error creating socket");
        return -1;
    }
cout << "socket created --- TCP PRINCIPAL" << endl;
     // Configuration de l'adresse du serveur TCP PRINCIPAL
    struct sockaddr_in serv_addr2;
    serv_addr2.sin_family = AF_INET;
    serv_addr2.sin_port = htons(PORT_TCP_PRINCIPAL);
    inet_aton(IP_ADDRESS, &serv_addr2.sin_addr);
    if(inet_aton(IP_ADDRESS, &serv_addr2.sin_addr)==0)
    {cout << "Error : could not convert IP_ADDRESS" << endl;}
cout << "adresse du TCP PRINCIPAL configuree" << endl;
    // Connexion au serveur TCP PRINCIPAL
    if (int i = connect(sockfd2, (struct sockaddr*) &serv_addr2, sizeof(serv_addr2)) < 0) {
        printf("Error connecting to server TCP PRINCIPAL, error code = %d\n", i);
        return -1;
    }
    printf("CONNECTE AU SERVEUR TCP PRINCIPAL\n");
    
    
    //SETUP SERVO
    wiringPiSetupGpio();
    // Setup with pinbase 300 and i2c location 0x40
    int fd = pca9685Setup(PIN_BASE, 0x40, HERTZ);
    if (fd < 0){
        printf("Error in SERVO-MOTORS setup\n");
        return fd;
    }
    // Reset all output
    pca9685PWMReset(fd);
    pwmWrite(PIN_BASE+1, 90);//INIT SERVO à 0 degrés
    int data = 0;

    //BOUCLE PRINCIPALE
    while(true){
cout << "debut boucle..." << endl;
        // Réception des données
        int n = read(newsockfd, &data, sizeof(data));
        if(n<0){
                cout << "Error reading data from socket" << endl;
        }
        else if(n==0){
                cout << "Client (BALISE) disconnected !" << endl;
                close(sockfd);
                break;
        }
        else{
                /*
                0 degres = 90
                45 degres = 190
                90 degres = 300
                135 degres = 400
                180 degres = 500
                */
                if(data==1){ //SI MVMT DETECTE : positionne servo sur 90 degres = 300
                        cout << "MOTION DETECTED --- SERVOMOTOR ON" << endl;
                        pwmWrite(PIN_BASE+1, 300);//90 degres
                        // Envoi de la détection au serveur TCP PRINCIPAL
                        //write(sockfd2, &data, sizeof(data));
                        delay(2000);
                        pwmWrite(PIN_BASE+1, 500);//180 degres apres mvmt
                        delay(500);
                }
		else{
                	//SI AUCUN MVMT : positionne servo sur 0 degres = 90
                	cout << "NO MOTION --- SERVOMOTOR OFF" << endl;
			pwmWrite(PIN_BASE+1, 90);
		}
		write(sockfd2, &data, sizeof(data));
        }
        }
    return 0;
}


