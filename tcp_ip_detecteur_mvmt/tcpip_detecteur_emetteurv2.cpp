//CODE POUR EMETTEUR
#include <iostream>
#include <fstream>
#include <ios>	
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>


#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>
#define PIR_PIN 17

using namespace std;

const char* IP_ADDRESS = "192.168.1.248"; // Adresse IP du Raspberry Pi maître
const int PORT = 8080; // Port utilisé pour la communication

int main() {
    // Création de la socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("Error creating socket");
        return -1;
    }
    
    // Configuration de l'adresse du serveur
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    inet_aton(IP_ADDRESS, &serv_addr.sin_addr);
    if(inet_aton(IP_ADDRESS, &serv_addr.sin_addr)==0)
    {cout << "Error : could not convert IP_ADDRESS" << endl;}
    
    // Connexion au serveur
    if (int i = connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
        printf("Error connecting to server, error code = %d\n", i);
	return -1;
    }
    printf("CONNECTE AU SERVEUR\n");
    
    //SET UP CONNECTION DETECTEUR MOUVMENT
    wiringPiSetupGpio();
    pinMode(PIR_PIN, INPUT);

    int data = 0;
    //BOUCLE INFINIE POUR ENVOYER DATA
    while (true) {
        // Lire les données du détecteur de mouvement
        if(digitalRead(PIR_PIN))
        {
         	data = 1;
		cout << "MOTION DETECTED" << endl;
        	// Envoi de la détection au serveur
        	write(sockfd, &data, sizeof(data));
        	sleep(1);
	}
    }

    // Fermeture de la socket
    //close(sockfd);

    return 0;
}
