#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string>
#include "pca9685.h"
#include "pca9685.c"
#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
#define PIN_BASE 300
#define HERTZ 50

using namespace std;

const int PORT = 8080; // Port utilisé pour la communication

int main() {
    // Création de la socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        std::cerr << "Error creating socket" << std::endl;
        return -1;
    }
    
	// Configuration de l'adresse locale
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    serv_addr.sin_addr.s_addr = INADDR_ANY;
    
	// Association de l'adresse à la socket
    if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << "Error binding socket" << std::endl;
        return -1;
    }

    // Mise en écoute de la socket
	listen(sockfd, 5);

	// Acceptation d'une connexion
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0) {
    	cerr << "Error accepting connection" << endl;
        return -1;
    }
    cout << "CONNECTION ACCEPTED" << endl;
    
    //SETUP SERVO
    wiringPiSetupGpio();
    // Setup with pinbase 300 and i2c location 0x40
    int fd = pca9685Setup(PIN_BASE, 0x40, HERTZ);
    if (fd < 0){
     	printf("Error in setup\n");
        return fd;
    }
    // Reset all output
    pca9685PWMReset(fd);
    
    int data = 0;
	
    while(true){
        // Réception des données
	int n = read(newsockfd, &data, sizeof(data));
        if(n<0){
		cout << "Error reading data from socket" << endl;
	}
	else if(n==0){
		cout << "Client disconnected !" << endl;
		close(sockfd);
		break;
	}
	else{
		cout << "DATA : " << data << endl;
		/*
		0 degres = 90
		45 degres = 190
		90 degres = 300
		135 degres = 400
		180 degres = 500
		*/
		if(data==1){ //SI MVMT DETECTE : positionne servo sur 90 degres = 300
        		cout << "MOTION DETECTED --- SERVOMOTOR ON" << endl;
       			pwmWrite(PIN_BASE+1, 300);//90 degres
        		delay(2000);
        		pwmWrite(PIN_BASE+1, 500);//180 degres apres mvmt
        		delay(500);
    		}
    		//SI AUCUN MVMT : positionne servo sur 0 degres = 90
        	//cout << "NO MOTION --- SERVOMOTOR OFF" << endl;
        	pwmWrite(PIN_BASE+1, 90);
    		

        }    
	}
    return 0;
}
