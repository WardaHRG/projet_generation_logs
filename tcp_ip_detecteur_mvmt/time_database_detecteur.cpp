#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <sstream>
#include <fstream>
#include <string.h>
#include <vector>
#include "sqlite3.h"
using namespace std;

int insertion_database(vector<string>);
//void split(const string, char, vector<string>);
void split(const string &message, char delimiteur, vector<string> &elements);

const int PORT = 3124; // Port utilisé pour la communication avec MASTER

int main() {
    // Création de la socket
    int sockfd3 = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd3 < 0) {
        std::cerr << "Error creating MASTER socket" << std::endl;
        return -1;
    }

        // Configuration de l'adresse locale
    struct sockaddr_in serv_addr3;
    serv_addr3.sin_family = AF_INET;
    serv_addr3.sin_port = htons(PORT);
    serv_addr3.sin_addr.s_addr = INADDR_ANY;

        // Association de l'adresse à la socket
    if (bind(sockfd3, (struct sockaddr *) &serv_addr3, sizeof(serv_addr3)) < 0) {
        std::cerr << "Error binding MASTER socket" << std::endl;
        return -1;
    }

    // Mise en écoute de la socket
        listen(sockfd3, 5);

        // Acceptation d'une connexion
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = accept(sockfd3, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0) {
        cerr << "Error accepting connection from MASTER" << endl;
        return -1;
    }
    cout << "CONNECTION FROM MASTER ACCEPTED" << endl;


    //int data = 0;
    char data[1024] = { 0 };
    //char elements[3];
    //vector<string> elements;

    while(true){
        // Réception des données
        int n = read(newsockfd, data, 1024);
        if(n<0){
                cout << "Error reading data from MASTER socket" << endl;
        }
        else if(n==0){
                cout << "Raspberry MASTER disconnected !" << endl;
                close(sockfd3);
                break;
        }
        else{
                /*
                0 degres = 90
                45 degres = 190
                90 degres = 300
                135 degres = 400
                180 degres = 500
                */
      		vector<string> elements;
      		if(data[0]=='1'){ //SI MVMT DETECTE
    			split(data, ';', elements);
                        cout << "---------------------------------" << endl <<
			     "MOTION DETECTED : " << data << endl;
			insertion_database(elements);
                }
		else{
 		//SI AUCUN MVMT
		split(data, ';', elements);
		cout << "---------------------------------" << endl <<
			     "NO MOTION : " << data << endl;
		insertion_database(elements);
		}
        }
        }
    return 0;
}


int insertion_database(vector<string> d){

    //PREPARATION DATABASE
    sqlite3 *db=NULL;
    int rc;
    sqlite3_stmt *stmt=NULL;
    rc = sqlite3_open("motionDataBase.db", &db);
    if (rc != SQLITE_OK) {
        printf("ERREUR Open : %s\n", sqlite3_errmsg(db));
        return 1;
    }
    
    //PREPARATION REQUETE
    char* requete = "INSERT INTO data values(null, ?, ?, ?)";

    if(sqlite3_prepare_v2(db, requete, -1, &stmt, NULL) != SQLITE_OK)
    {
        printf("ERREUR REQUETE\n");
    }

    //STEP REQUETE
    if(d[0]=="1"){
	    sqlite3_bind_text(stmt, 3, "MOTION DETECTED", strlen("MOTION DETECTED"), NULL);
    }
    if(d[0]=="0"){
	    sqlite3_bind_text(stmt, 3, "NO motion", strlen("NO motion"), NULL);
    }
    
    const char* date = (d[1]).c_str();
    sqlite3_bind_text(stmt, 1, date, strlen(date), NULL);
    const char* heure = (d[2]).c_str();
    sqlite3_bind_text(stmt, 2, heure, strlen(heure), NULL);

    rc = sqlite3_step(stmt);
    
    //FINALIZE + CLOSE DB
    sqlite3_finalize(stmt);
    rc = sqlite3_close(db);
    if (rc != SQLITE_OK) {
        printf("ERREUR CLOSE : %s\n", sqlite3_errmsg(db));
       return 1;
    }

    cout << "INSERTION DATABASE OK" << endl;
    return 0;
}





void split(const string &message, char delimiteur, vector<string> &elements)
{
    stringstream ss(message);
    string sousChaine;
    while (getline(ss, sousChaine, delimiteur))
    {
	 elements.push_back(sousChaine);
    }
}

