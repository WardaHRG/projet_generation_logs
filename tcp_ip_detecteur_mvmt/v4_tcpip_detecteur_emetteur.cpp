//CODE POUR EMETTEUR
#include <iostream>
#include <fstream>
#include <ios>	
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <ctime>
#include <sstream>
#include <cstring>
#include <stdlib.h>
#include <stdio.h>
#include <wiringPi.h>
#define PIR_PIN 17

using namespace std;

const char* IP_ADDRESS = "192.168.1.248"; // Adresse IP du Raspberry Pi maître
const int PORT = 8080; // Port utilisé pour la communication

int main() {
    // Création de la socket
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        printf("Error creating socket");
        return -1;
    }
    
    // Configuration de l'adresse du serveur
    struct sockaddr_in serv_addr;
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
    inet_aton(IP_ADDRESS, &serv_addr.sin_addr);
    if(inet_aton(IP_ADDRESS, &serv_addr.sin_addr)==0)
    {cout << "Error : could not convert IP_ADDRESS" << endl;}
    
    // Connexion au serveur
    if (int i = connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0) {
        printf("Error connecting to server, error code = %d\n", i);
	return -1;
    }
    printf("CONNECTED TO MASTER\n");
    
    //SET UP CONNECTION DETECTEUR MOUVMENT
    wiringPiSetupGpio();
    pinMode(PIR_PIN, INPUT);

    int state = 0;
    string message;

    //BOUCLE INFINIE POUR ENVOYER DATA
    while (true) {
	
	//CREATION DATE + HEURE
	time_t now = time(0);
        tm* localt = localtime(&now);
        ostringstream oss;
	ostringstream oss1;
        oss  << localt->tm_mday << "/" << localt->tm_mon + 1 << "/" << localt->tm_year + 1900;
	oss1 << localt->tm_hour << ":" << localt->tm_min << ":" << localt->tm_sec ;

	string time_str = oss.str() + ";" + oss1.str();
	
	// Lire les données du détecteur de mouvement
        if(digitalRead(PIR_PIN))
        {
		message = "1;" + time_str;
		char* message_ch = &message[0];
		cout << "MOTION DETECTED : " << message_ch << endl;
        	// Envoi de la détection au serveur
        	write(sockfd, message_ch, strlen(message_ch));
        	sleep(2);
		state = 1;
		while(state==1)
		{
			if(!digitalRead(PIR_PIN))
			{
				state = 0;
			}
		}
	}
	else{
		message = "0;" + time_str;
		char* msg_ch = &message[0];
		cout << "NO MOTION : " << msg_ch << endl;
		write(sockfd, msg_ch, strlen(msg_ch));
    		sleep(2);
		state = 1;
		while(state==1)
		{
			if(digitalRead(PIR_PIN))
			{
				state = 0;
			}
		}
	}
    }
    // Fermeture de la socket
    //close(sockfd);

    return 0;
}
