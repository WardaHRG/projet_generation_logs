#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <string>
//#include "pca9685.h"
//#include "pca9685.c"
//#include <wiringPi.h>
#include <stdio.h>
#include <stdlib.h>
//#define PIN_BASE 300
//#define HERTZ 50

using namespace std;

const int PORT = 3124; // Port utilisé pour la communication avec MASTER

int main() {
    // Création de la socket
    int sockfd3 = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd3 < 0) {
        std::cerr << "Error creating MASTER socket" << std::endl;
        return -1;
    }

        // Configuration de l'adresse locale
    struct sockaddr_in serv_addr3;
    serv_addr3.sin_family = AF_INET;
    serv_addr3.sin_port = htons(PORT);
    serv_addr3.sin_addr.s_addr = INADDR_ANY;

        // Association de l'adresse à la socket
    if (bind(sockfd3, (struct sockaddr *) &serv_addr3, sizeof(serv_addr3)) < 0) {
        std::cerr << "Error binding MASTER socket" << std::endl;
        return -1;
    }

    // Mise en écoute de la socket
        listen(sockfd3, 5);

        // Acceptation d'une connexion
    struct sockaddr_in cli_addr;
    socklen_t clilen = sizeof(cli_addr);
    int newsockfd = accept(sockfd3, (struct sockaddr *) &cli_addr, &clilen);
    if (newsockfd < 0) {
        cerr << "Error accepting connection from MASTER" << endl;
        return -1;
    }
    cout << "CONNECTION FROM MASTER ACCEPTED" << endl;


    int data = 0;

    while(true){
        // Réception des données
        int n = read(newsockfd, &data, sizeof(data));
        if(n<0){
                cout << "Error reading data from MASTER socket" << endl;
        }
        else if(n==0){
                cout << "Client (MASTER) disconnected !" << endl;
                close(sockfd3);
                break;
        }
        else{
                /*
                0 degres = 90
                45 degres = 190
                90 degres = 300
                135 degres = 400
                180 degres = 500
                */
                if(data==1){ //SI MVMT DETECTE
                        cout << "MOTION DETECTED" << endl;
                        
                }
		else{
 		//SI AUCUN MVMT
                cout << "NO MOTION" << endl;
		}
        }
        }
    return 0;
}
